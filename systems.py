import numpyro.distributions as dist
from framework.components import CPU, Generic, Network, Queue, ReadWrite


def case_study():
    fileservice = ReadWrite('fileservice', lambda _: (245, 650))
    networkservice = Network('networkservice', lambda _: (530, 1660))
    compute = CPU('compute', lambda _: (240, 360))

    read_file = Generic('readfile',
                        lambda comp_times: dist.Normal(comp_times[0], 10),
                        components=[fileservice],
                        exclude=True)
    write_file = Generic('writefile',
                         lambda comp_times: dist.Normal(comp_times[0], 25),
                         components=[fileservice],
                         exclude=True)

    download = Generic('download',
                       lambda comp_times: dist.Normal(comp_times[0], 10),
                       components=[networkservice],
                       exclude=True)
    upload = Generic('upload',
                     lambda comp_times: dist.Normal(comp_times[0], 25),
                     components=[networkservice],
                     exclude=True)

    return Generic('case_study',
                   lambda comp_times: dist.Normal(sum(comp_times), 50),
                   components=[read_file, download, compute, upload, write_file],
                   exclude=True)


def system3_convert():
    fileservice = ReadWrite('fileservice', lambda _: (500, 700))
    convert = CPU('convert', lambda _: (200, 400))

    read_file = Generic('readfile',
                        lambda comp_times: dist.Normal(comp_times[0], 10),
                        components=[fileservice],
                        exclude=True)
    write_file = Generic('writefile',
                         lambda comp_times: dist.Normal(sum(comp_times), 10),
                         components=[convert, fileservice],
                         exclude=True)

    return Generic('convert_endpoint',
                   lambda comp_times: dist.Normal(sum(comp_times), 25),
                   components=[read_file, write_file],
                   exclude=True)


def system2_pop():
    task_queue = Queue('queue', lambda _: (100, 200))
    file_read = ReadWrite('readfile', lambda _: (600, 1000))
    setup = Generic('setup',
                    lambda comp_samples: dist.Normal(
                        comp_samples[0] + comp_samples[1], 100),
                    components=[task_queue, file_read],
                    exclude=True)
    return CPU('pop_endpoint', lambda comp_samples: (comp_samples[0], comp_samples[0]*2.0), components=[setup])


def system1_encode():
    asset_download = Network('download', lambda _: (720, 1100))
    encoding = CPU('encoding', lambda _: (1200, 2400))
    output_write = ReadWrite('write', lambda _: (300, 600))
    return Generic('encode_endpoint', lambda comp_times: dist.Normal(sum(comp_times), 33),
                   components=[asset_download, encoding, output_write], exclude=True)


def encoding_endpoint():
    load = Network('load', lambda _: (520, 780))
    encode = CPU('encode', lambda _: (1200, 2400))
    write = ReadWrite('write', lambda _: (700, 1000))
    return Generic('encoding_endpoint', lambda comp_times: dist.Normal(sum(comp_times), 100), components=[load, encode, write], exclude=True)
