import math
from typing import List
import numpy as np
import numpyro
import numpyro.distributions as npdist
from jax import random
from systems import system1_encode, system2_pop, system3_convert, encoding_endpoint, case_study
import jax.numpy as jnp

from framework.components import Generic


def set_slow_components(node: Generic, slow_endpoints: List[str], visited: List[Generic] = []) -> None:
    if node not in visited and node.exclude == False:
        node.set_slow(node.name in slow_endpoints)
        visited.append(node)

    for comp in node.components:
        set_slow_components(comp, slow_endpoints, visited)


def generate_response_times(root: Generic, num_samples: int, slow_endpoints: List[str] = [], filename=None):
    print('Generating', num_samples, 'response times for', root.name, 'with bottlenecks', slow_endpoints)
    times = []
    set_slow_components(root, slow_endpoints)
    for _ in range(0, num_samples):
        time = numpyro.sample('response_time', root.get_dist())
        times.append(time.item())

    if filename is not None:
        np.savetxt(filename, [times], delimiter=", ", fmt='%.2f')
    return times


if __name__ == "__main__":
    key = random.PRNGKey(1337)
    with numpyro.handlers.seed(rng_seed=key):
        system = case_study()
        slow_components = ['networkservice']
        response_times = generate_response_times(system, 100, slow_components,)
        # f"{system.name}/{''.join(slow_components)}_slow2.csv")
        observed_times = np.array(response_times)
        observed = (round(np.mean(observed_times), 2), round(np.std(observed_times), 2))
        print('observed:', observed[0], observed[1])
