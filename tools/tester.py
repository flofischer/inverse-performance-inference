from datetime import datetime
import sys
from typing import List
import numpy as np
import pandas as pd
from framework.components import Generic
from framework.ipi import InversePerformanceInference, createFolderIfNecessary
from systems import system3_convert, system2_pop, system1_encode, encoding_endpoint, case_study


def run_tests(system: Generic, components: List[Generic], num_steps: int = 1000, num_warmup: int = 100, num_chains: int = 1):
    test_results = []
    for component in components:
        output_filename = f"../data/{system.name}/{num_chains}chains/{component}_slow_{num_steps}.{num_warmup}"
        print(f'Reading input for "{system.name}" with component "{component}"')
        data = pd.read_csv(f'../data/{system.name}/{component}_slow.csv', delimiter=',',
                           dtype=np.float32, header=None)
        observed_times = np.array(data.values[0])

        start = datetime.now()
        print(f'Performing inference on "{system.name}" with component "{component}"')
        ipi = InversePerformanceInference(system, observed_times)
        ipi.train(num_steps, num_warmup, num_chains)

        print(f'Evaluating results for "{system.name}" with component "{component}"')
        samples, results, sites = ipi.get_results(plot=False, filename=f"{output_filename}.csv")

        runtime = datetime.now()-start
        print("Runtime:", runtime)

        runtimefilename = f"{output_filename}_runtime.txt"
        createFolderIfNecessary(runtimefilename)
        np.savetxt(runtimefilename, np.array([str(runtime)]), delimiter="", newline="", fmt="%s")

        ipi.plot_samples_for_sites(samples,
                                   sites,
                                   f"{num_chains}chains {component} Histograms",
                                   filename=f"{output_filename}_hists.png")
        ipi.plot_traces_for_sites(samples,
                                  sites,
                                  f"{num_chains}chains {component} Traces",
                                  filename=f"{output_filename}_traces.png",
                                  binning=10)

        predicted_site = max(results, key=lambda e: e['mean'])['site']
        test_results.append(predicted_site[:predicted_site.index("_slow_chance")])
    return test_results


def get_components(node: Generic, components: List[str] = []) -> List[Generic]:
    if node.name not in components and node.exclude == False:
        components.append(node.name)
    for comp in node.components:
        components = get_components(comp, components)
    return components


if __name__ == "__main__":
    sys_name = sys.argv[2] if len(sys.argv) > 2 else None
    if sys_name is not None:
        system = next(sys for sys in [system1_encode(), system2_pop(),
                      system3_convert(), case_study()] if sys.name == sys_name)
    else:
        system = case_study()
    components = get_components(system)

    num_steps = int(sys.argv[3]) if len(sys.argv) > 3 else 500
    num_warmup = int(sys.argv[4]) if len(sys.argv) > 4 else 100
    num_chains = int(sys.argv[1]) if len(sys.argv) > 1 else 3

    test_results = run_tests(system, components, num_steps, num_warmup, num_chains)
    print("Tested:", components)
    print("Results:", test_results)
