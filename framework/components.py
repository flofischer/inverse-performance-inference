
import math
from typing import Callable, NewType, List, Tuple
import numpyro.distributions as dist
import jax.numpy as jnp
import numpyro


Generic = NewType('Generic', 'Generic')


class Generic:
    def __init__(self, name: str, func: Callable[..., float], components: List[Generic] = [], is_slow=False, slow_chance_prior=dist.Uniform(0, 1), exclude=False) -> None:
        self.name = name
        self.func = func
        self.components = components
        self.is_slow = is_slow
        self.slow_chance_prior = slow_chance_prior
        self.exclude = exclude

    def get_dist(self):
        if len(self.components) > 0:
            return self.func([comp.sample(self.name) for comp in self.components])
        return self.func(self.is_slow)

    def sample(self, name_suffix: str | None = None):
        distribution = self.get_dist()
        return numpyro.sample(f'{self.name + (f"_{name_suffix}" if name_suffix is not None else "")}_response_time', distribution)

    def add_component(self, comp: Generic) -> None:
        self.components.append(comp)

    def set_slow(self, is_slow: bool) -> None:
        self.is_slow = is_slow


class CPU(Generic):
    def __init__(self, name: str, mode: Callable[..., Tuple[int]], spread: Tuple[int] = (0.25, 0.5), is_slow=False, components: List[Generic] = [], slow_chance_prior=dist.Uniform(0, 1), exclude=False) -> None:
        def calc_mean(m, s): return jnp.log(m) + jnp.square(s)

        def func(arg):
            modes = mode(arg)
            return dist.LogNormal(
                jnp.where(self.is_slow, calc_mean(modes[1], spread[1]), calc_mean(modes[0], spread[0])),
                jnp.where(self.is_slow, spread[1], spread[0]))

        super().__init__(name, func, is_slow=is_slow, components=components, slow_chance_prior=slow_chance_prior, exclude=exclude)


class Network(Generic):
    def __init__(self, name: str, mode: Callable[..., Tuple[int]], spread: Tuple[int] = (0.1, 0.2), is_slow=False, components: List[Generic] = [], slow_chance_prior=dist.Uniform(0, 1), exclude=False) -> None:
        def calc_mean(m, s): return jnp.log(m) + jnp.square(s)

        def func(arg):
            modes = mode(arg)
            return dist.LogNormal(jnp.where(self.is_slow, calc_mean(modes[1], spread[1]), calc_mean(modes[0], spread[0])),
                                  jnp.where(self.is_slow, spread[1], spread[0]))
        super().__init__(name, func, is_slow=is_slow, components=components, slow_chance_prior=slow_chance_prior, exclude=exclude)


class NetworkPareto(Generic):
    def __init__(self, name: str, mean: Callable[..., Tuple[int]], alpha: Tuple[int] = (7, 7), is_slow=False, components: List[Generic] = [], slow_chance_prior=dist.Uniform(0, 1), exclude=False) -> None:
        def calc_scale(m, a): return m*(a-1)/a

        def func(arg):
            means = mean(arg)
            return dist.LogNormal(
                jnp.where(self.is_slow, calc_scale(means[1], alpha[1]), calc_scale(means[0], alpha[0])),
                jnp.where(self.is_slow, alpha[1], alpha[0]))
        super().__init__(name, func, is_slow=is_slow, components=components, slow_chance_prior=slow_chance_prior, exclude=exclude)


class Memory(Generic):
    def __init__(self, name: str, mean: Callable[..., Tuple[int]], beta: Tuple[int] = (1.5, 3), is_slow=False, components: List[Generic] = [], slow_chance_prior=dist.Uniform(0, 1), exclude=False) -> None:
        def calc_scale(m, b): return m / jnp.gamma(1 + 1/b)

        def func(arg):
            means = mean(arg)
            return dist.Weibull(
                jnp.where(self.is_slow, calc_scale(means[1], beta[1]), calc_scale(means[0], beta[0])),
                jnp.where(self.is_slow, beta[1], beta[0]))
        super().__init__(name, func, is_slow=is_slow, components=components, slow_chance_prior=slow_chance_prior, exclude=exclude)


class Queue(Generic):
    def __init__(self, name: str, mean: Callable[..., Tuple[int]], is_slow=False, components: List[Generic] = [], slow_chance_prior=dist.Uniform(0, 1), exclude=False) -> None:
        def func(arg):
            means = mean(arg)
            return dist.Exponential(jnp.where(self.is_slow, 1/means[1], 1/means[0]))
        super().__init__(name, func, is_slow=is_slow, components=components, slow_chance_prior=slow_chance_prior, exclude=exclude)


class ReadWrite(Generic):
    def __init__(self, name: str, mode: Callable[..., Tuple[int]], spread: Tuple[int] = (0.5, 0.5), is_slow=False, components: List[Generic] = [], slow_chance_prior=dist.Uniform(0, 1), exclude=False) -> None:
        def calc_mean(m, s): return math.log(m) + s**2

        def func(arg):
            modes = mode(arg)
            return dist.LogNormal(
                jnp.where(self.is_slow, calc_mean(modes[1], spread[1]), calc_mean(modes[0], spread[0])),
                jnp.where(self.is_slow, spread[1], spread[0]))
        super().__init__(name, func, is_slow=is_slow, components=components, slow_chance_prior=slow_chance_prior, exclude=exclude)
