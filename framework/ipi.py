import math
import os
from typing import Any, List
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
import numpy as np
import numpyro
import numpyro.distributions as dist
from numpyro.infer import MCMC, NUTS, Predictive
from framework.components import Generic
import jax.numpy as jnp
from jax import random

numpyro.set_platform('cpu')
np.set_printoptions(formatter={'float_kind': "{:.2f}".format})


def createFolderIfNecessary(filename):
    if not os.path.isdir(filename[:filename.rfind("/")]):
        os.makedirs(filename[:filename.rfind("/")])


def print_summary(sites, samples):
    results = []
    for key in sites:
        results.append({'site': key, 'mean': np.round(np.mean(samples[key]), 4),
                        'std': np.round(np.std(samples[key]), 4),
                        'median': np.round(np.median(samples[key]), 4),
                        '68%-quantile': [np.round(val, 4) for val in np.quantile(samples[key], [0.32, 0.68])]})
    dataframe = pd.DataFrame(results)
    print(dataframe)
    return dataframe, results


class InversePerformanceInference:
    def __init__(self, system_root: Generic, data: Any, rng_seed: Any = random.PRNGKey(1337)):
        self.system_root = system_root
        self.data = data
        self.mcmc = None
        self.rng_seed = rng_seed
        self.inner_components: List[Generic] = []

    def set_slow_components(self, node: Generic) -> None:
        if node not in self.inner_components and node.exclude == False:
            slow_chance = numpyro.sample(f'{node.name}_slow_chance', node.slow_chance_prior)
            slow_chance = jnp.where(slow_chance < 0, 0, slow_chance)
            slow_chance = jnp.where(slow_chance > 1, 1, slow_chance)
            slow = numpyro.sample(f'{node.name}_slow', dist.Bernoulli(slow_chance), infer={'enumerate': 'parallel'})
            node.set_slow(jnp.where(slow == 1, True, False))
            self.inner_components.append(node)

        for comp in node.components:
            self.set_slow_components(comp)

    def model(self, root: Generic, observed: Any):
        self.inner_components = []
        self.set_slow_components(root)
        with numpyro.plate("observed", len(observed)):
            return numpyro.sample('response_time', root.get_dist(), obs=observed)

    def train(self, num_samples, num_warmup=None, num_chains=1, kernel=None, target_accept_prob=0.8):
        if num_warmup is None:
            num_warmup = int(num_samples * 0.1)
        if kernel is None:
            kernel = NUTS(self.model, step_size=0.01, adapt_step_size=True,
                          find_heuristic_step_size=True, target_accept_prob=target_accept_prob, init_strategy=numpyro.infer.init_to_median())
        # kernel = HMC(model, step_size=1, num_steps=1)#0.0855, num_steps=4)

        self.mcmc = MCMC(kernel, num_warmup=num_warmup, num_samples=num_samples,
                         num_chains=num_chains, chain_method='sequential')
        with numpyro.handlers.seed(rng_seed=self.rng_seed):
            self.mcmc.run(numpyro.prng_key(), self.system_root, self.data)
        return

    def get_results(self, components: List[str] | None = None, plot=True, title='Results', filename=None):
        if self.mcmc is None:
            print('MCMC hasn\'t been run yet.')
            return

        samples = self.mcmc.get_samples()

        if components is None:
            components = [comp.name for comp in self.inner_components]

        sites = [key for key, _ in samples.items() if key.endswith(
            'slow_chance') and key[:key.index('_slow_chance')] in components]

        dataframe, results = print_summary(sites, samples)
        if filename is not None:
            createFolderIfNecessary(filename)
            dataframe.to_csv(filename, index=False, encoding='utf-8')

        observed = (round(np.mean(self.data), 2), round(np.std(self.data), 2))
        print('observed:', observed[0], observed[1])

        if len(self.system_root.components) > 0 and self.system_root.exclude:
            keys = [key for key, _ in samples.items() if key[:key.index(
                '_')] in [comp.name for comp in self.system_root.components] and key.endswith('response_time')]
            with numpyro.handlers.seed(rng_seed=self.rng_seed):
                response_times = []

                for i in range(0, len(self.data)):
                    comp_samples = np.array([samples[key][i][i] for key in keys])
                    time = self.system_root.func(comp_samples).sample(numpyro.prng_key(), (1,))
                    response_times.append(time)

            posterior = (round(np.mean(response_times), 2), round(np.std(response_times), 2))
            print('posterior:', posterior[0], posterior[1])
            print('diff:', round(observed[0]-posterior[0], 2), round(observed[1]-posterior[1], 2))

        if plot:
            self.plot_samples_for_sites(samples, sites, title)
            self.plot_traces_for_sites(samples, sites, title)

        return samples, results, sites

    def get_predictive_samples(self, sites, plot=True, title=None):
        if self.mcmc is None:
            print('MCMC hasn\'t been run yet.')
            return

        posterior_samples = self.mcmc.get_samples()
        predictive = Predictive(self.model, posterior_samples=posterior_samples, return_sites=sites)
        samples = predictive(random.split(self.rng_seed)[1], self.system_root, self.data)

        post_sites = [key for key, _ in posterior_samples.items() if not key.endswith('response_time')]
        print_summary(post_sites, samples)

        if plot:
            self.plot_samples_for_sites(samples, sites, title=title if title is not None else 'Posterior results')

        return samples

    def plot_samples_for_sites(self, samples, sites, title='Results', filename=None):
        plot_rows = math.floor(len(sites)/2)
        plot_cols = math.ceil(len(sites)/plot_rows)
        _, axes = plt.subplots(plot_rows, plot_cols)
        axes = axes.flatten()

        for i, site in enumerate(sites):
            sns.histplot(samples[site], kde=False, label=str(site), ax=axes[i])
            axes[i].legend()
            axes[i].set_xlim(0.0, 1.0)
        plt.title(title)
        if filename is not None:
            createFolderIfNecessary(filename)
            plt.gcf().set_size_inches(17, 5)
            plt.savefig(filename, dpi=300)
        else:
            plt.show()

    def plot_traces_for_sites(self, samples, sites, title="Traces", filename=None, binning=20):
        plot_cols = math.floor(len(sites)/2)
        plot_rows = math.ceil(len(sites)/plot_cols)
        _, axes = plt.subplots(plot_rows, plot_cols)
        axes = axes.flatten()
        plt.title(title, loc="right")

        for i, param in enumerate(sites):
            axes[i].set_ylim(-0.1, 1.1)
            axes[i].set_yticks([0.0, 0.25, 0.5, 0.75, 1.0])
            averaged = np.mean(samples[param].reshape(-1, binning), axis=1)
            axes[i].plot(averaged, label=param)
            axes[i].legend()

        if filename is not None:
            createFolderIfNecessary(filename)
            plt.gcf().set_size_inches(9, 7)
            plt.savefig(filename, dpi=300)
        else:
            plt.show()
