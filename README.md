# Inverse Performance Inference

This repository contains work implemented for the thesis "Bayesian inference on inverse software performance problems" and provides a framework for performing Markov Chain Monte Carlo inference on models defined using the framework's provided templates.
The templates provided represent common computer systems' components and a respective predefined performance behaviour.
This framework can be used to model performance problems and perform inference on said models.