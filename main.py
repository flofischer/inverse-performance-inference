import time
import jax.numpy as jnp
import numpyro
from systems import system3_convert, system2_pop, system1_encode, encoding_endpoint, case_study
from framework.ipi import InversePerformanceInference
from jax import random
import numpy as np
import sys
import pandas as pd
import tools.generator as generator
from datetime import datetime


def main():
    root_node = case_study()

    inputfile = sys.argv[1] if len(
        sys.argv) > 1 else f'./data/{root_node.name}/{root_node.components[0].name}_slow.csv'
    print("Reading file", inputfile)
    data = pd.read_csv(f'{inputfile}', delimiter=',', dtype=np.float32, header=None)
    observed_times = np.array(data.values[0])

    num_samples = 1000
    num_chains = 3
    num_warmup = 300

    start = datetime.now()

    ipi = InversePerformanceInference(root_node, observed_times)
    ipi.train(num_samples, num_warmup, num_chains, target_accept_prob=0.9)
    samples, _, sites = ipi.get_results(plot=False, title=inputfile)

    end = datetime.now()
    print("Runtime:", (end-start))

    ipi.plot_samples_for_sites(samples, sites, f"{inputfile} Histograms")
    ipi.plot_traces_for_sites(samples, sites, f"{inputfile} Traces")


if __name__ == "__main__":
    main()
